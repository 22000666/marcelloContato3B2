//
//  ViewController.swift
//  marcelloContato3B2
//
//  Created by COTEMIG on 11/08/22.
//

import UIKit

struct Contato {
    let nome: String
    let telefone: String
    let email: String
    let endereco: String
}

class ViewController: UIViewController, UITableViewDataSource {
    var listaDeContatos:[Contato] = []
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaDeContatos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MinhaCelula", for: indexPath) as! MyCell
        let contato = listaDeContatos[indexPath.row]
        
        cell.nome.text = contato.nome
        cell.telefone.text = contato.telefone
        cell.email.text = contato.email
        cell.endereco.text = contato.endereco
        
        return cell
    }
    
    
    @IBOutlet weak var tableContatos: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableContatos.dataSource = self
        
        listaDeContatos.append(Contato(nome: "Marcello", telefone: "12313", email: "marcello@gmail.com", endereco: "Rua Bananas Vermelhas"))
        listaDeContatos.append(Contato(nome: "Lucas", telefone: "8923432", email: "lucas@gmail.com", endereco: "Rua Carro Azul"))
        listaDeContatos.append(Contato(nome: "Felipe", telefone: "16576577", email: "felipe@gmail.com", endereco: "Rua Verdes"))
    }


}

